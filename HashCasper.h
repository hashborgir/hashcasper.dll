#pragma once
#include "DLLmain.h"
#include "D2Ptrs.h"

void __stdcall ReduceDurability(D2UnitStrc* pPlayer);

BOOL __stdcall D2Common_CharmRequirements(D2UnitStrc* pItem, D2UnitStrc* pOwner);
