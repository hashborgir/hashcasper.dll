#include "HashCasper.h"

void __stdcall ReduceDurability(D2UnitStrc* pPlayer)
{
	D2COMMON_STATLIST_AddUnitStat(pPlayer, UNIT_DURABILITY, -10, 0);
}

//Rewriting D2Common.#10840
BOOL __stdcall D2Common_CharmRequirements(D2UnitStrc* pItem, D2UnitStrc* pOwner) {
    if (pItem == NULL || pOwner == NULL)
        return FALSE;
    if (!D2COMMON_ITEMS_CheckItemTypeId(pItem, 0x0D))
        return FALSE;
    D2ItemDataStrc* pCharm = pItem->pItemData;
    if ((pItem->dwUnitType == UNIT_ITEM) && (pCharm != 0) && (D2COMMON_GetItemNodePage(pItem) == NODEPAGE_STORAGE)) {
        D2StaticPathStrc* pItemPath = pItem->pStaticPath;
        if ((pItemPath->nYPos >= 0) && (pItemPath->nYPos <= 3)) {
            if (ITEMS_CheckRequirements(pItem, pOwner, 0, 0, 0, 0))
                return TRUE;
        }
    }
    return FALSE;
}

//Display Red background for Charm when not in charm zone
//Check if Charm, if so call D2Common.#10840, otherwise use regular call
BOOL __stdcall D2Client_CharmRequirements(D2UnitStrc* pItem, D2UnitStrc* pOwner, BOOL p1, BOOL* p2, BOOL* p3, BOOL* p4) {
    if (D2COMMON_ITEMS_CheckItemTypeId(pItem, 0x0D))
        return D2Common_CharmRequirements(pItem, pOwner);
    return ITEMS_CheckRequirements(pItem, pOwner, p1, p2, p3, p4);
}